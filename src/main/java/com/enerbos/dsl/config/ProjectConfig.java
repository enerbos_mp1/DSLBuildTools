package com.enerbos.dsl.config;

import com.enerbos.driver.kafka.producer.EnerbosProducer;
import com.enerbos.driver.kafka.producer.EnerbosProducerImpl;
import com.enerbos.dsl.config.properties.DriverCoreKafkaConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-23 11:09
 * @Description
 */
@Configuration
@EnableConfigurationProperties({DriverCoreKafkaConfigProperties.class})
public class ProjectConfig {
    @Autowired
    private DriverCoreKafkaConfigProperties driverCoreKafkaConfigProperties;

    @Bean
    public EnerbosProducer enerbosProducer() {
        EnerbosProducerImpl enerbosProducer = new EnerbosProducerImpl(driverCoreKafkaConfigProperties.getBrokerAddress());
        enerbosProducer.setTopic(driverCoreKafkaConfigProperties.getTopic());
        return enerbosProducer;
    }
}
