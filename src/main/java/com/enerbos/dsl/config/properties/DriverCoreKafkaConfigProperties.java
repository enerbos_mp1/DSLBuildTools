package com.enerbos.dsl.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-23 11:10
 * @Description
 */
@ConfigurationProperties(prefix = "enerbos.driver.kafka")
public class DriverCoreKafkaConfigProperties {

    private String topic = "default";
    private String brokerAddress = "localhost:9092";
    private String zookeeperConnect = "localhost:2181";

    public DriverCoreKafkaConfigProperties() {}

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBrokerAddress() {
        return brokerAddress;
    }

    public void setBrokerAddress(String brokerAddress) {
        this.brokerAddress = brokerAddress;
    }

    public String getZookeeperConnect() {
        return zookeeperConnect;
    }

    public void setZookeeperConnect(String zookeeperConnect) {
        this.zookeeperConnect = zookeeperConnect;
    }
}