package com.enerbos.dsl.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-23 10:12
 * @Description
 */
@Entity
public class CalculationConfig {
    public CalculationConfig() {}

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid.hex")
    @Column(name = "id", unique = true, nullable = false, length = 36)
    private String id;

    @Column
    private String name;
    @Column
    private String ruleId;
    @Column
    private String ruleName;
    @Column
    private String ruleBody;
    @Column
    private String expression;
    @Column
    private String params;

    /**
     * 当前计算配置使用的模板
     */
    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn
    private CalculationTemplate calculationTemplate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleBody() {
        return ruleBody;
    }

    public void setRuleBody(String ruleBody) {
        this.ruleBody = ruleBody;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public CalculationTemplate getCalculationTemplate() {
        return calculationTemplate;
    }

    public void setCalculationTemplate(CalculationTemplate calculationTemplate) {
        this.calculationTemplate = calculationTemplate;
    }

    @Override
    public String toString() {
        return "CalculationConfig{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", ruleId='" + ruleId + '\'' +
                ", ruleName='" + ruleName + '\'' +
                ", ruleBody='" + ruleBody + '\'' +
                ", expression='" + expression + '\'' +
                ", params='" + params + '\'' +
                ", calculationTemplate=" + calculationTemplate +
                '}';
    }
}
