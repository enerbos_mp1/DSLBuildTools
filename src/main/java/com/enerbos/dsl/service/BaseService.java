package com.enerbos.dsl.service;

import com.enerbos.dsl.model.CalculationConfig;
import com.enerbos.dsl.model.CalculationTemplate;

import java.util.List;
import java.util.Map;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-23 10:31
 * @Description
 */
public interface BaseService {

    List<CalculationTemplate> getCalculationTemplateList();

    List<CalculationConfig> getCalculationConfigList(String name);

    String buildConfig(String templateId, String name, String expression, String expressionTemplate, String orgId, String siteId, List<String> params);

    String sendConfig(String templateId, String name, String expression, String expressionTemplate, String orgId, String siteId, List<String> params);

    List<Map<String,Object>> getCompanyInfoList(String filter, Integer pageNum, Integer pageSize);

}
