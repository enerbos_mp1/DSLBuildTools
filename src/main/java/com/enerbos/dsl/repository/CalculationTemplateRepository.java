package com.enerbos.dsl.repository;

import com.enerbos.dsl.model.CalculationTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-23 10:29
 * @Description
 */
@Repository
public interface CalculationTemplateRepository extends JpaRepository<CalculationTemplate, String>, JpaSpecificationExecutor<CalculationTemplate> {

}
