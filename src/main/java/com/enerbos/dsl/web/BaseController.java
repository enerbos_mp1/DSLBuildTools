package com.enerbos.dsl.web;

import com.enerbos.common.feedbackmsg.SystemMessage;
import com.enerbos.dsl.model.CalculationConfig;
import com.enerbos.dsl.model.CalculationTemplate;
import com.enerbos.dsl.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-22 19:34
 * @Description
 */
@RestController
public class BaseController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BaseService baseService;

    @RequestMapping(value = "/tool/common/company/list", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SystemMessage companyList(@RequestParam(name = "filter", required = false) String filter,
                                     @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                                     @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
        SystemMessage retMsg = new SystemMessage();
        try {
            List<Map<String, Object>> result = baseService.getCompanyInfoList(filter, pageNum, pageSize);

            retMsg.setSuccess(true);
            retMsg.setData(result);
        } catch (Exception e) {
            retMsg.setSuccess(false);
            retMsg.setDetailMsg(String.format("Get Company List Error : [%s].", e.getMessage()));
            logger.error(retMsg.getDetailMsg());
        }

        return retMsg;
    }

    @RequestMapping(value = "/tool/calc/template/list", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SystemMessage calcTemplateList() {
        SystemMessage retMsg = new SystemMessage();
        try {
            List<CalculationTemplate> result = baseService.getCalculationTemplateList();

            retMsg.setSuccess(true);
            retMsg.setData(result);
        } catch (Exception e) {
            retMsg.setSuccess(false);
            retMsg.setDetailMsg(String.format("Get Template List Error : [%s].", e.getMessage()));
            logger.error(retMsg.getDetailMsg());
        }

        return retMsg;
    }

    @RequestMapping(value = "/tool/calc/config/list", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SystemMessage calcConfigList(@RequestParam(name = "name", required = false) String name) {
        SystemMessage retMsg = new SystemMessage();
        try {
            List<CalculationConfig> result = baseService.getCalculationConfigList(name);

            retMsg.setSuccess(true);
            retMsg.setData(result);
        } catch (Exception e) {
            retMsg.setSuccess(false);
            retMsg.setDetailMsg(String.format("Get Config List Error : [%s].", e.getMessage()));
            logger.error(retMsg.getDetailMsg());
        }

        return retMsg;
    }

    @RequestMapping(value = "/tool/calc/process/buildConfig", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SystemMessage buildConfig(@RequestParam(name = "templateId") String templateId,
                                     @RequestParam(name = "name") String name,
                                     @RequestParam(name = "expression") String expression,
                                     @RequestParam(name = "orgId") String orgId,
                                     @RequestParam(name = "siteId") String siteId,
                                     @RequestParam(name = "expressionTemplate", required = false) String expressionTemplate,
                                     @RequestParam(name = "params", required = false) List<String> params) {
        SystemMessage retMsg = new SystemMessage();
        try {
            String result = baseService.buildConfig(templateId, name, expression, expressionTemplate, orgId, siteId, params);

            retMsg.setSuccess(true);
            retMsg.setData(result);
        } catch (Exception e) {
            retMsg.setSuccess(false);
            retMsg.setDetailMsg(String.format("BuildConfig Error : [%s].", e.getMessage()));
            logger.error(retMsg.getDetailMsg());
        }

        return retMsg;
    }

    @RequestMapping(value = "/tool/calc/publish/sendConfig", method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SystemMessage sendConfig(@RequestParam(name = "templateId") String templateId,
                                     @RequestParam(name = "name") String name,
                                     @RequestParam(name = "expression") String expression,
                                     @RequestParam(name = "orgId") String orgId,
                                     @RequestParam(name = "siteId") String siteId,
                                     @RequestParam(name = "expressionTemplate", required = false) String expressionTemplate,
                                     @RequestParam(name = "params", required = false) List<String> params) {
        SystemMessage retMsg = new SystemMessage();
        try {
            String result = baseService.sendConfig(templateId, name, expression, expressionTemplate, orgId, siteId, params);

            retMsg.setSuccess(true);
            retMsg.setData(result);
        } catch (Exception e) {
            retMsg.setSuccess(false);
            retMsg.setDetailMsg(String.format("BuildConfig Error : [%s].", e.getMessage()));
            logger.error(retMsg.getDetailMsg());
        }

        return retMsg;
    }

}
