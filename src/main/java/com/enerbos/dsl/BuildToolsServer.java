package com.enerbos.dsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * All rights Reserved, Designed By 翼虎能源
 * Copyright:    Copyright(C) 2015-2017
 * Company   北京翼虎能源科技有限公司
 *
 * @author hk
 * @version 1.0
 * @date 2017-02-22 19:33
 * @Description
 */
@EnableCaching
@SpringBootApplication
@EntityScan(basePackages = {"com.enerbos.dsl", "com.enerbos.authorise"})
@ComponentScan(basePackages = {"com.enerbos.dsl", "com.enerbos.authorise"})
@EnableJpaRepositories(
        basePackages = {"com.enerbos.dsl", "com.enerbos.authorise"},
        repositoryFactoryBeanClass = com.enerbos.spring.DefaultRepositoryFactoryBean.class)
public class BuildToolsServer {

    public static void main(String[] args) {
        SpringApplication.run(BuildToolsServer.class, args);
    }

}
